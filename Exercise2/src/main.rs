// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 0 { 
        return 0;
    }
    if n == 1 {
        return 1 
    }
    if n == 2 {
        return 1 
    }
    let mut first = 1;
    let mut second = 1;
    let mut result = 0;
    
    for i in 3..n+1 { 
        result = first + second;
        first = second;
        second = result;
    }
    result
}


fn main() {
    println!("{}", fibonacci_number(10));
    println!("{}", fibonacci_number(1));
    println!("{}", fibonacci_number(2));
    println!("{}", fibonacci_number(4));
    println!("{}", fibonacci_number(7));
}
